<?php


namespace App\Controller;


use App\Entity\Link;
use App\Form\Type\LinkType;
use App\Form\ConfirmActionType;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpClient\HttpClient;



class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Link::class);

        $linkEntries = $repository->findAll();
        return $this->render('default/index.html.twig', [
            'linkEntries' => $linkEntries,
            'shortLinkBase' => $this->getParameter('uri.shortener.api.url'),
        ]);
    }

    /**
     * @Route("/success", name="task_success")
     */
    public function taskSuccess()
    {
        return $this->render('default/indexPopupSuccess.html.twig', []);
    }

    /**
     * @Route("/add", name="add_link_entry_popup")
     */
    public function addLinkEntryPopup(Request $request)
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)

        $link = new Link();

        $form = $this->createForm(LinkType::class, $link);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $link = $form->getData();

            $client = HttpClient::create();
            $url = $this->getParameter('uri.shortener.api.url').$this->getParameter('uri.shortener.api.prefix');
            // $url should be of this format http://{server_ip}:{server_port}/{api_prefix}
            // e.g. http://127.0.0.1:8001/api/v1/
            $url = $url."uri";
            try {
                $response = $client->request('POST', $url, [
                    'json' => ['base_uri' => $link->getShortLink(), 'target_uri' => $link->getTargetLink()],
                ]);
            } catch (\Exception  $e) {
                return new Response("Error: Impossible to query the API.");
            }

            try {
                $contents = $response->toArray();
            } catch (ClientException  $e) {
                $contents = $response->toArray(false);
                if ($contents['error'] === "base_uri already taken") {
                    $form->get('shortlink')->addError(new FormError('This Short link cannot be used. Use another one.'));
                    return $this->render(
                        'default/indexPopup.html.twig', [
                        'form' => $form->createView(),
                        'title' => "Add new entry",
                    ]);

                }
                return new Response("Error: Impossible to query the API.<br>Unspecified error.");
            }
            if ($contents['base_uri']===$link->getShortLink() && $contents['target_uri']===$link->getTargetLink()) {

            } else {
                return new Response(
                    "Error: Values are not consistent.<br>".
                    "New values have been saved.<br>Close window and reload page."
                );
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();

            return $this->redirectToRoute('task_success');
        }

        return $this->render(
            'default/indexPopup.html.twig', [
            'form' => $form->createView(),
            'title' => "Add new entry",
        ]);
    }

    /**
     * @Route("/edit/{entry_id}", name="edit_link_entry_popup")
     */
    public function editLinkEntryPopup($entry_id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Link::class);

        $link = $repository->find($entry_id);
        $linkClone = clone $link;

        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)

        $form = $this->createForm(LinkType::class, $link);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = HttpClient::create();
            $url = $this->getParameter('uri.shortener.api.url').$this->getParameter('uri.shortener.api.prefix');
            // $url should be of this format http://{server_ip}:{server_port}/{api_prefix}
            // e.g. http://127.0.0.1:8001/api/v1/
            $url = $url."uri";

            $url = $url."/".$linkClone->getShortLink();

            $link = $form->getData();

            if ($linkClone->getShortLink() === $link->getShortLink() ) {
                try {
                    $response = $client->request('PUT', $url, [
                        'json' => ['base_uri' => $link->getShortLink(), 'target_uri' => $link->getTargetLink()],
                    ]);
                } catch (\Exception  $e) {
                    return new Response("Error: Impossible to query the API.");
                }

                try {
                    $contents = $response->toArray();
                } catch (ClientException  $e) {
                    $contents = $response->toArray(false);
                    if ($contents['error'] === "This URI isn't registered. Please, check your spelling.") {
                        return new Response(
                            "Error: This link ins't registered in URI shortener.<br>".
                            "Please, close the window and delete this entry."
                        );
                    }
                    return new Response("Error: Impossible to query the API.<br>Unspecified error.");
                }
                if ($contents['base_uri']===$link->getShortLink() && $contents['target_uri']===$link->getTargetLink()) {

                } else {
                    return new Response(
                        "Error: Values are not consistent.<br>".
                        "New values have been saved.<br>Close window and reload page."
                    );
                }

            } else {
                $link->setShortLink($linkClone->getShortLink());
                $form = $this->createForm(LinkType::class, $link);
                $form->get('shortlink')->addError(new FormError('The Short link cannot be changed. Create a new item instead.'));
                return $this->render(
                    'default/indexPopup.html.twig', [
                    'form' => $form->createView(),
                    'title' => "Add new entry",
                ]);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();

            return $this->redirectToRoute('task_success');
        }

        return $this->render(
            'default/indexPopup.html.twig', [
            'form' => $form->createView(),
            'title' => "Edit entry \"".$link->getTitle()."\"",
        ]);
    }

    /**
     * @Route("/delete/{entry_id}", name="delete_link_entry_popup")
     */
    public function deleteLinkEntryPopup($entry_id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Link::class);

        $link = $repository->find($entry_id);

        $form = $this->createForm(ConfirmActionType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = HttpClient::create();
            $url = $this->getParameter('uri.shortener.api.url').$this->getParameter('uri.shortener.api.prefix');
            // $url should be of this format http://{server_ip}:{server_port}/{api_prefix}
            // e.g. http://127.0.0.1:8001/api/v1/
            $url = $url."uri";

            $url = $url."/".$link->getShortLink();


            try {
                $response = $client->request('DELETE', $url);
                $contents = $response->toArray();
            } catch (ClientException  $e) {
                $contents = $response->toArray(false);
                if ($contents['error']==="This URI isn't registered. Please, check your spelling.") {
                    $entityManager = $this->getDoctrine()->getManager();

                    $entityManager->remove($link);
                    $entityManager->flush();
                    return $this->redirectToRoute('task_success');
                }
                return new Response("Error: Impossible to query the API.<br>Unspecified error.");
            }

            if ($contents['deleted']==="true") {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->remove($link);
                $entityManager->flush();

                return $this->redirectToRoute('task_success');
            }

            return new Response("Error: Deletion failed. Unspecified error");
        }

        return $this->render('default/indexPopupDelete.html.twig', [
            'form' => $form->createView(),
            'title' => $link->getTitle(),
        ]);
    }
}