<?php

namespace App\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use App\Repository\LinksRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LinksRepository::class)
 * @UniqueEntity("shortlink")
 */
class Link
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotNull
     * @Assert\Type("string")
     * @Assert\Length(max = 32)
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotNull
     * @Assert\Type("string")
     * @Assert\Length(max = 200)
     * @Assert\NotBlank
     */
    private $targetlink;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotNull
     * @Assert\Type("string")
     * @Assert\Length(max = 64)
     * @Assert\NotBlank
     */
    private $shortlink;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max = 250)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTargetlink(): ?string
    {
        return $this->targetlink;
    }

    public function setTargetlink(string $targetlink): self
    {
        $this->targetlink = $targetlink;

        return $this;
    }

    public function getShortlink(): ?string
    {
        return $this->shortlink;
    }

    public function setShortlink(string $shortlink): self
    {
        $this->shortlink = $shortlink;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
